-module(mod_search).
-behaviour(gen_mod).

-export([start/2, stop/1]).
-include("jlib.hrl").
-include("ejabberd.hrl").

-export([get_records/5,
         get_bid_records/4,
        get_records_all/4,
        get_records_next/6,
        get_comments_records/4,
        get_offers_all/3,
        get_comments_all/4,
        get_records_number/1]).


start(Host, _Opts) ->
    ?INFO_MSG("starting mod_search v1", [Host]).
stop(Host) ->
    ?INFO_MSG("stoping mod_seach v1", [Host]).

get_records_number(Name) ->
         mod_buckets:get_number(Name).

%Get All offers by auction tag
get_offers_all(Bucket, Type, Auction) ->
    {ok, Pid } = mod_buckets:connect(),
    {C, Result} =  case Type of
                binary ->
                    riakc_pb_socket:get_index_eq(Pid, 
                        list_to_binary(Bucket),
                        {binary_index, "auction"}, 
                        list_to_binary(Auction))
            end,
    FinalResult = case C of
        error ->
            "error";
         _->
            {_,Items,_,_}  = Result,
            Items
    end,
    mod_buckets:disconnect(Pid),
    FinalResult.

get_comments_all(Bucket, Tag, Type, Auction) ->
    {ok, Pid } = mod_buckets:connect(),
    {C, Result} =  case Type of
                binary ->
                    riakc_pb_socket:get_index_eq(Pid, 
                        list_to_binary(Bucket),
                        {binary_index, Tag}, 
                        list_to_binary(Auction))
            end,
    FinalResult = case C of
        error ->
            "error";
         _->
            {_,Items,_,_}  = Result,
            Items
    end,
    mod_buckets:disconnect(Pid),
    FinalResult.

get_records_all(Bucket, Tag, Type, Location) ->
    {ok, Pid } = mod_buckets:connect(),
    {C, Result} =  case Type of
                binary ->
                    riakc_pb_socket:get_index_eq(Pid, 
                        list_to_binary(Bucket),
                        {binary_index, Tag}, 
                        list_to_binary(Location))
            end,
    FinalResult = case C of
        error ->
            "error";
         _->
            {_,Items,_,_}  = Result,
            I = case Items of
                []-> "";
                _->
                    mod_auctions:tuplelement_to_list(Items,iq,binary)
            end,
            I
    end,
    mod_buckets:disconnect(Pid),
    FinalResult.


get_records(Bucket, Tag, Type, Amount, Criteria)->
    {ok, Pid } = mod_buckets:connect(),
    {C, Result} =  case Type of 
                        binary -> riakc_pb_socket:get_index_eq(Pid, 
                                    list_to_binary(Bucket), 
                                    {binary_index, Tag},
                                    list_to_binary(Criteria),
                                    [{max_results, Amount}])
                    end,
    FinalResult = case C of 
        error ->
            {"error" , "bad_request"};
        _->
            {_,Items,_,Continuation}  = Result,
            R = case Continuation of
                undefined ->
                    {mod_auctions:tuplelement_to_list(Items,message,binary), 
                        "end"};
                _->
                    {mod_auctions:tuplelement_to_list(Items,message,binary), 
                        binary_to_list(Continuation)}
            end,
            R
    end,
    mod_buckets:disconnect(Pid),
    FinalResult.

get_records_next(Bucket, Tag, Type, Amount, Continuation, Criteria)->
    {ok, Pid } = mod_buckets:connect(),
    {C , Result} =  case Type of
                        binary -> riakc_pb_socket:get_index_eq(Pid,
                                 list_to_binary(Bucket),
                                 {binary_index, Tag},
                                 list_to_binary(Criteria),
                                 [{max_results, Amount},
                                    {continuation, list_to_binary(Continuation)}])
                    end,
    FinalResult = case C of 
        error ->
            {"error" , "bad_request"};
        _->
            {_,Items,_,Continuation2}  = Result,
            R = case Continuation2 of
                undefined ->
                    {mod_auctions:tuplelement_to_list(Items,message,binary), "end"};
                _->
                    {mod_auctions:tuplelement_to_list(Items,message,binary), binary_to_list(Continuation2)}
            end,
            R
    end,
    mod_buckets:disconnect(Pid),
    FinalResult.


get_bid_records(Auction, _TimeStamp, _Amount, _Continuation)->
        {ok, Pid} = mod_buckets:connect(),
        {ok , Result} =  riakc_pb_socket:get_index_eq(Pid, list_to_binary(Auction++"-bidding-history"),
                                                        {binary_index, "bid-history"},
                                                        list_to_binary(Auction)),
       mod_buckets:disconnect(Pid),
       {_,Items,_,_}  = Result,
       Items.


get_bids_record_by_amount(Auction, TimeStamp, _Amount, _Continuation)->
    {ok, Pid} = mod_buckets:connect(),

    case _Continuation of
        [] ->
            {ok , Result} =  riakc_pb_socket:get_index_eq(Pid, list_to_binary(Auction++"-bidding-history"),
                                                  {binary_index, "bid-history"},
                                                  list_to_binary(Auction)),
            mod_buckets:disconnect(Pid),
            {_,Items,_,_}  = Result,
            Items;
        C ->
            C
    end.

%%Tag auction for all 
get_comments_records(Auction, _TimeStamp, _Amount, _Continuation)->
    {ok, Pid} = mod_buckets:connect(),
    case _Continuation of
        [] ->
            {ok , Result} =  riakc_pb_socket:get_index_eq(Pid, list_to_binary(Auction++"-comment-history"),
                                                  {binary_index, "auction"},
                                                  list_to_binary(Auction)),
            mod_buckets:disconnect(Pid),
            {_,Items,_,_}  = Result,
            Items;
        C ->
            C
    end.
