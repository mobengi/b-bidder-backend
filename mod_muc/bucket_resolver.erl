-module(bucket_resolver).

-define(NODE_IP, "146.185.156.173").
-define(NODE_PORT, 8087).
-export([set_bucket/3]).

connect()->
    riakc_pb_socket:start_link(?NODE_IP, ?NODE_PORT).

set_bucket(Name, undefined ,  Data)->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    Obj = riakc_obj:new(N, undefined, Data),
    riakc_pb_socket:put(Pid, Obj);

set_bucket(Name, Key, Data) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    Obj = riakc_obj:new(N, K, Data),
    riakc_pb_socket:put(Pid, Obj).


