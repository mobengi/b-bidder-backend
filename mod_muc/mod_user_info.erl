-module(mod_user_info).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").
-export([start/2, stop/1, 
        process_iq_tok/3]).

-define(SERVER, "gimmick.fi").
-define(AJID, "conference.gimmick.fi").
-define(TOKENS, "user-token").

start(Host, Opts) ->
    IQDisc = gen_mod:get_opt(iqdisc, Opts, parallel),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?TOKENS, ?MODULE, process_iq_tok, IQDisc).
        
stop(Host) ->
    gen_iq_handler:remove_iq_handler(ejabberd_local, Host, ?TOKENS).

process_iq_tok(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            User = xml:get_tag_cdata(xml:get_subtag(SubEl, "user")),
            Token = xml:get_tag_cdata(xml:get_subtag(SubEl, "token")),
            Platform = xml:get_tag_cdata(xml:get_subtag(SubEl, "platform")),

            case Platform of 
                "an" ->
                        case mod_buckets:update_check_bucket(list_to_binary(User++"token"),
                                list_to_binary("android"),
                                check,
                                "",
                                one) of
                            "" -> 
                                mod_buckets:set_bucket(list_to_binary(User++"token"),
                                    list_to_binary("android"),
                                    list_to_binary(Token));
                            _->
                                mod_buckets:update_check_bucket(list_to_binary(User++"token"),
                                    list_to_binary("android"),
                                    update_value,
                                    list_to_binary(Token),
                                    "")
                        end;
                ip ->
                       case mod_buckets:update_check_bucket(list_to_binary(User++"token"),
                                list_to_binary("iphone"),
                                check,
                                "",
                                one) of
                            "" -> 
                                mod_buckets:set_bucket(list_to_binary(User++"token"),
                                    list_to_binary("iphone"),
                                    list_to_binary(Token));
                            _->
                                mod_buckets:update_check_bucket(list_to_binary(User++"token"),
                                    list_to_binary("iphone"),
                                    update_value,
                                    list_to_binary(Token),
                                    "")
                        end
            end,                         
            IQ#iq{type = result, xmlns = ?TOKENS,
                sub_el = [SubEl]};
        get ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]}
    end.
