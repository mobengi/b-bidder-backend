-module(mod_users).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").
-export([start/2, stop/1]).
-export([get_user_profile/3,set_user_profile/3]).
%%IQ names-spaces
-define(GET_PROFILE,"getuser-profile").
-define(SET_PROFILE,"setuser-profile").
-define(SERVER,"gimmick.fi").

start(_Host, _Opts) ->
    IQDisc = gen_mod:get_opt(iqdisc, _Opts, one_queue),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?SET_PROFILE, ?MODULE, set_user_profile, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?GET_PROFILE, ?MODULE, get_user_profile, IQDisc),
    ok.

stop(_Host) ->
    ok.
set_user_profile(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            UTAG = xml:get_tag_attr_s("userid", SubEl),
            UNTAG = xml:get_tag_attr_s("username", SubEl),
            mod_profile_manager:update_profile_item_data(UTAG, username, UNTAG),
            Username = mod_profile_manager:get_item(UTAG, username),
            IQ#iq{type = result, xmlns = ?SET_PROFILE ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?SET_PROFILE}],
                        [{xmlelement, "username", [],[{xmlcdata,Username }]}
                        ]}]
                        
            };
        get->
            ignore
    end.

get_user_profile(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get->
            ?INFO_MSG("ASKKKKK ~p~n",[SubEl]),
            UTAG = xml:get_tag_attr_s("userid", SubEl),
            
            Username = mod_profile_manager:get_item(UTAG, username),
            Lastseen = case mod_profile_manager:get_item(UTAG, lastseen)of
                "empty" ->
                    "0";
                [] ->
                    "0";
                Last ->
                    Last
            end,
            AOWN  = case mod_profile_manager:get_items(UTAG, auctionsown) of
                "empty" ->
                    "0";
                [] ->
                    "0";
                CountO ->
                    CountO
            end,
            AFOLLOWING =  case mod_profile_manager:get_items(UTAG, auctionsfollowing) of
                "empty" ->
                    "0";
                [] ->
                    "0";
                CountF ->
                    CountF
            end,

            ?INFO_MSG("ASKKKKK ~p~n",[UTAG]),
            ?INFO_MSG("ASKKKKK ~p~n",[Lastseen]),
            ?INFO_MSG("ASKKKKK ~p~n",[AOWN]),
            ?INFO_MSG("ASKKKKK ~p~n",[AFOLLOWING]),
            ?INFO_MSG("ASKKKKK ~p~n",[Username]),
            IQ#iq{type = result, xmlns = ?GET_PROFILE ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?GET_PROFILE}],
                        [{xmlelement, "username", [],[{xmlcdata,Username }]},
                         {xmlelement, "userid", [],[{xmlcdata, UTAG }]},
                        {xmlelement, "lastseen", [],[{xmlcdata, Lastseen }]},
                         {xmlelement, "auctions-created", [],[{xmlcdata, AOWN }]},
                         {xmlelement, "acutions-following", [],[{xmlcdata, AFOLLOWING }]}
                        ]}]
                        
            }

    end.
                    


