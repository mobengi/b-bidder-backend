-module(mod_notifier).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").

-export([start/2, stop/1]).
-export([notify_post/2, 
        notify_video/2, 
        notify_change_auction/1,
        notify_new_auction/1,
        notify_video_request/2,
        notify_offer/2]).
-define(DOMAIN, "gimmick.fi").
-define(NODE_IP, "localhost").
-define(POST_TOPIC, "post-").
-define(VIDEO_TOPIC, "video-").
-define(NEW_AUCTION, "newauction").
-define(AUCTION_UPDATE, "updateauction").
-define(AUCTION_OFFER, "offer-").
-define(REQUEST_VIDEO, "videorequest-").
start(_Host, _Opts) ->
    ?INFO_MSG("MQTT NOTIFIER", []),
    application:start(emqttc),
    ?INFO_MSG("MQTT NOTIFIER Started", []),
    ok.

stop(_Host) ->
    ?INFO_MSG("MQTT NOTIFIER", []),
    ok.

notify_offer(Client, Packet) ->
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, {client_id, list_to_binary(Client)}]),
    TopicPost = ?AUCTION_OFFER++Client,
    emqttc:subscribe(C, list_to_binary(TopicPost), qos0),
    emqttc:publish(C, list_to_binary(TopicPost),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

notify_video_request(Client, Packet) ->
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, {client_id, list_to_binary(Client)}]),
    TopicPost = ?REQUEST_VIDEO++Client,
    emqttc:subscribe(C, list_to_binary(TopicPost), qos0),
    emqttc:publish(C, list_to_binary(TopicPost),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

notify_post(Client, Packet) ->
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, {client_id, list_to_binary(Client)}]),
    TopicPost = ?POST_TOPIC++Client,
    emqttc:subscribe(C, list_to_binary(TopicPost), qos0),
    emqttc:publish(C, list_to_binary(TopicPost),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

notify_video(Client, Packet) ->
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, {client_id, list_to_binary(Client)}]),
    TopicPost = ?VIDEO_TOPIC++Client,
    emqttc:subscribe(C, list_to_binary(TopicPost), qos0),
    emqttc:publish(C, list_to_binary(TopicPost),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

notify_change_auction(Packet) ->
    ClientId = ?DOMAIN++"_"++get_random_string(32, "qwertyuiopasdfghjklzxcvbnm1234567890"),
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, 
                                 {client_id, list_to_binary(ClientId)}]),
    TopicChangeAuction = ?AUCTION_UPDATE,
    emqttc:subscribe(C, list_to_binary(TopicChangeAuction), qos0),
    emqttc:publish(C, list_to_binary(TopicChangeAuction),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

notify_new_auction(Packet) ->
    ClientId = ?DOMAIN++"_"++get_random_string(32, "qwertyuiopasdfghjklzxcvbnm1234567890"),
    {ok, C} = emqttc:start_link([{host, ?NODE_IP}, 
                                 {client_id, list_to_binary(ClientId)}]),
    TopicNewAuction = ?NEW_AUCTION,
    emqttc:subscribe(C, list_to_binary(TopicNewAuction), qos0),
    emqttc:publish(C, list_to_binary(TopicNewAuction),list_to_binary(Packet)),
    %% receive message
    receive
    {publish, Topic, Payload} ->
        ?INFO_MSG("Message Received from ~s: ~p~n", [Topic, Payload])
    after
    1000 ->
        io:format("Error: receive timeout!~n")
    end,
    %% disconnect from broker
    emqttc:disconnect(C).

get_random_string(Length, AllowedChars) ->
        lists:foldl(fun(_, Acc) ->
                [lists:nth(random:uniform(length(AllowedChars)),
                           AllowedChars)]
                ++ Acc
        end, [], lists:seq(1, Length)).
