-module(mod_auction_video).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").
-export([start/2, stop/1]).
-export([start_video/3,
         request_video/3,
         stop_video/3]).
%%IQ names-spaces
-define(START_VIDEO,"startauction-video").
-define(SERVER,"gimmick.fi").
-define(STOP_VIDEO,"stopauction-video").
-define(REQUEST_VIDEO,"request-video").

start(_Host, _Opts) ->
    IQDisc = gen_mod:get_opt(iqdisc, _Opts, one_queue),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?STOP_VIDEO, ?MODULE, stop_video, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?START_VIDEO, ?MODULE, start_video, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?REQUEST_VIDEO, ?MODULE, request_video, IQDisc),
    ok.

stop(_Host) ->
    ok.
start_video(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            PacketSend = {xmlelement, "iq",[{"type", "result"}],[SubEl]},
            ?INFO_MSG("ASKKKKK ~p~n",[SubEl]),

            %%Update video
            case mod_auction_manager:get_item(ATAG,"video") of
                "empty" -> 
                    mod_auction_manager:update_auction_data(ATAG,video,"on");
                [] ->
                    mod_buckets:set_bucket(ATAG,"video",list_to_binary("on"));
                "off" ->
                    mod_auction_manager:update_auction_data(ATAG,video,"on");
                _->
                    ignore
            end,
            %%%GET PARTICIPANTS
            PData=mod_auction_manager:get_items(ATAG, participants),
            List = [ I || I <- string:tokens(PData,",")],
            lists:foreach(fun(P) ->
                             ?INFO_MSG("ASKKKKK ~p~n",[P]),
                            User = P++"@"++?SERVER++"/"++P,
                            mod_notifier:notify_video(P,xml:element_to_string(PacketSend)),
                            ejabberd_router:route(jlib:string_to_jid(?SERVER),
                                              jlib:string_to_jid(User),
                                              PacketSend)
                end, List),
            IQ#iq{type = result, xmlns = ?START_VIDEO,
            sub_el = [SubEl]};
        get->
            ignore
    end.


request_owner_video(Auction, El) ->
    Packet = {xmlelement, "iq",[{"type", "result"}],[El]},
    %%%GET OWNER
    Owner = mod_auction_manager:get_item(Auction, "owner_id"),
    OwnerJid = Owner++"@"++?SERVER++"/"++Owner,
    mod_notifier:notify_video_request(Owner,xml:element_to_string(Packet)),
     ejabberd_router:route(jlib:string_to_jid(?SERVER),
                          jlib:string_to_jid(OwnerJid),
                          Packet).    

request_video(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
    set ->
        IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
    get ->
        ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
        request_owner_video(ATAG, SubEl),
        IQ#iq{type = result, xmlns = ?REQUEST_VIDEO,
            sub_el = [SubEl]}
    end.

stop_video(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            PacketSend = {xmlelement, "iq",[{"type", "result"}],[SubEl]},

            %%Update video
            case mod_auction_manager:get_item(ATAG,"video") of
                "empty" -> 
                    mod_auction_manager:update_auction_data(ATAG,video,"off");
                [] ->
                    mod_buckets:set_bucket(ATAG,"video",list_to_binary("off"));
                "on" ->
                    mod_auction_manager:update_auction_data(ATAG,video,"off");
                _->
                    ignore
            end,
            %%%GET PARTICIPANTS
            PData=mod_auction_manager:get_items(ATAG, participants),
            List = [ I || I <- string:tokens(PData,",")],
            lists:foreach(fun(P) ->
                             ?INFO_MSG("ASKKKKK ~p~n",[P]),
                            User = P++"@"++?SERVER++"/"++P,
                            mod_notifier:notify_video(P,xml:element_to_string(PacketSend)),
                            ejabberd_router:route(jlib:string_to_jid(?SERVER),
                                              jlib:string_to_jid(User),
                                              PacketSend)
                end, List),
            IQ#iq{type = result, xmlns = ?STOP_VIDEO,
            sub_el = [SubEl]};
        get->
            ignore
    end.
