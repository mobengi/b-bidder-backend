-module(mod_auctions).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").
-export([start/2, stop/1, 
        tuplelement_to_list/3,
        strip_qot/1,
        get_post_data/1,
        get_auction_info/3,
        get_auction_packet/1,
        process_iq_usr_auctions/3,
        process_iq_search_auctions/3,
        process_iq_search_auction_info_list/3,
        process_iq_search_auctions_all/3,
        process_iq_filter_search/3,
        process_iq_check_status/3,
        auto_join_auctions/1,
        auto_join_auctions_res/1,
        extract_username/1,
        extract_aname/1,
        list_to_term/1,
        delete_auction/3,
        increment_views/1,
        retreive_auction_comments/3,
        process_iq_bid/3,
        process_iq_like/3,
        process_iq_post/3,
        process_iq_view/3,
        filter_in_category_maxmin_loc/6,
        filter_in_category_loc/4,
        filter_in_category_maxmin/5,
        get_auction_comment_history/2]).

%%User auction namespace
-define(DELETEAUCTION, "delete-auction").
-define(POSTAUCTIONS,"auction-post").
-define(UAUCTIONS, "user-auctions").
-define(SAUCTION, "asearch").
-define(SDAUCTION ,"alistsearch").
-define(FILTERS , "filtersearch").
-define(CHKAUCTIONS, "check-auction").
-define(INFOAUCTION, "auction-info").
%%Expected keys
-define(AUCTIONS_KEY, "auctions").
-define(SERVER, "gimmick.fi").
-define(AJID, "conference.gimmick.fi").
-define(ANEW, "last-aprice").
-define(ALIKE, "auction-like").
-define(AVIEW, "auction-view").
-define(ACOMMENTHISTORY, "auction-comment-history").
start(Host, Opts) ->
    ?INFO_MSG("starting auctions module  v0.0.1", []),
    IQDisc = gen_mod:get_opt(iqdisc, Opts, one_queue),
    ejabberd_hooks:add(user_available_hook, Host,
                        ?MODULE, auto_join_auctions, 50),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?POSTAUCTIONS, ?MODULE, process_iq_post, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?ACOMMENTHISTORY, ?MODULE, retreive_auction_comments, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?ANEW, ?MODULE, process_iq_bid, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?CHKAUCTIONS, ?MODULE, process_iq_check_status, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?UAUCTIONS, ?MODULE, process_iq_usr_auctions, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?SAUCTION, ?MODULE, process_iq_search_auctions, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?SDAUCTION, ?MODULE, process_iq_search_auction_info_list, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?FILTERS, ?MODULE, process_iq_filter_search, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?AVIEW, ?MODULE, process_iq_view, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?ALIKE, ?MODULE, process_iq_like, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?INFOAUCTION, ?MODULE, get_auction_info, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, Host, ?DELETEAUCTION, ?MODULE, delete_auction, IQDisc).
stop(Host) ->
    application:stop(erlcron),
    gen_iq_handler:remove_iq_handler(ejabberd_local, Host, ?UAUCTIONS).

list_to_term(String) ->
    S = list_to_atom(String),
    S.


strip_qot(S)->
	F = fun(To_stripp, Strip_with) -> 
	lists:filter( fun(C) -> not lists:member(C, Strip_with) end, To_stripp ) 
	end,
	F(S, "\"").

str_to_term(S)->
    %%{ok, T, _} = erl_scan:string(S++" "),
    {ok, T, _} = erl_scan:string(S++"."),
    case erl_parse:parse_term(T) of
        {ok, Term} ->
            Term;
        {error, Error} ->
            Error
    end.

get_list_auctions_loop(L)->
    T = lists:foldl(fun(X,A2) ->
                {_,_,[{_,A},{_,_}],_}  = X ,
                lists:append([A2,[A]])
        end, [], L),
    T.

get_list_auctions(L)->
    T = lists:foldl(fun(X,A2) ->
                {_,_,[{_,A},{_,_}],_}  = X ,
                lists:append([A2,[extract_aname(A)]])
        end, [], L),
    T.

tuplelement_to_list(L,M,TP)->
        T = lists:foldl(fun(X,A2) ->
                    E = case TP of 
                        binary -> 
                            C = X,
                            binary_to_list(C);
                        _->
                            {C} = X,
                            C
                    end,    
                    lists:append([A2,[E]])
            end, [], L),
    F = case M of
        iq -> string:join(T,",");
        message ->
            T
    end,
    F.
extract_aname(JID) ->
    [J | _] = string:tokens(JID, "@"),
    J.
extract_username(JID) ->
        [J | _] = string:tokens(jlib:jid_to_string(JID), "@"),
        J.
filter_auctions(L1,L2) ->
    [ X || X <- L1, lists:member(X,L2) =/= true ]. 


delete_auction(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        get ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        set ->
            Auction = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            %%TODO IMPLEMENTE NOTIFIER FOR PARTICIPANTS 
            mod_auction_manager:delete_auction_data(Auction),
	    PacketDelete= {xmlelement, "iq",[{"type", "result"}],
                        [
                            {xmlelement, "auction", [{"xmlns",?DELETEAUCTION}], 
                                [{xmlcdata, Auction}]}
                        ]
                    },
            mod_notifier:notify_change_auction(xml:element_to_string(PacketDelete)),

            IQ#iq{type = result, xmlns = ?DELETEAUCTION,
                sub_el =[{xmlelement, "auction", [{"xmlns", ?DELETEAUCTION}], 
                        [{xmlcdata,  Auction}]}]}
    end.

process_iq_usr_auctions(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
            U = xml:get_tag_attr_s("user", SubEl),
            Auctions = mod_buckets:update_check_bucket(U,
                list_to_binary(?AUCTIONS_KEY), 
                check, "", some),

            IQ#iq{type = result, xmlns = ?UAUCTIONS,
                sub_el =[{xmlelement, "query", [{"xmlns", ?UAUCTIONS},{"user", U}], 
                        [{xmlcdata,  tuplelement_to_list(Auctions,iq,binary)}]}]}
    end.

send_post_to_owner(ATag,El) ->
 Packet = {xmlelement, "iq",[{"type", "result"}],[El]},
 %%%GET OWNER 
 Owner = mod_auction_manager:get_item(ATag, "owner_id"),
 OwnerJid = Owner++"@"++?SERVER++"/"++Owner,
 mod_notifier:notify_post(Owner,xml:element_to_string(Packet)),
 ejabberd_router:route(jlib:string_to_jid(?SERVER), 
                          jlib:string_to_jid(OwnerJid), 
                          Packet).
                        

send_post_to_participants(ATag, BTag, El)->
 Packet = {xmlelement, "iq",[{"type", "result"}],[El]},
            %%%GET PARTICIPANTS
            PData=mod_auction_manager:get_items(ATag, participants),
            List = [ I || I <- string:tokens(PData,",")],
            lists:foreach(fun(P) ->
                    case P of
                        BTag ->
                            ignore;
                        _->
                            User = P++"@"++?SERVER++"/"++P,    
                            mod_notifier:notify_post(P,xml:element_to_string(Packet)), 
                            ejabberd_router:route(jlib:string_to_jid(?SERVER), 
                                              jlib:string_to_jid(User), 
                                              Packet)
                    end
                end, List).

check_participants(Auction,Bidder) ->
    case mod_buckets:update_check_bucket(Auction, "participants", check, [], one) of
        [] -> 
            mod_buckets:update_check_bucket(Auction,"participants", update, Bidder,[]);
        _->
            case mod_muc_room:check_participant(Bidder,Auction) of
                false ->
                    mod_muc_room:add_participant(Auction, Bidder);
                true  ->
                    ignore
            end
    end.

process_iq_post(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            BTag = xml:get_tag_cdata(xml:get_subtag(SubEl, "bidder")),
            ATag = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            BDTag = xml:get_tag_cdata(xml:get_subtag(SubEl, "body")),
            Username =  mod_profile_manager:get_item(BTag, username),
            Time = integer_to_list(mod_auction_triggers:get_timestamp()),
            mod_auction_manager:store_comment(ATag, BDTag ,BTag ,Time),
            {D1,D2,D3,Data} = SubEl,
            Data1 = {xmlelement,
              "time",[],
              [{xmlcdata, Time}]},
             Data2 = {xmlelement,
              "username",[],
              [{xmlcdata, Username}]},
            Data3 = [Data1] ++ Data,
            Data4 = [Data2] ++ Data3,
            SubElFinal = {D1,D2,D3,Data4},
            case mod_auction_manager:get_item(ATag,"owner_id") of
                BTag ->                  
                    send_post_to_participants(ATag, BTag, SubElFinal),
                    IQ#iq{type = result,  xmlns =?POSTAUCTIONS,
                          sub_el = [SubElFinal]};
                Other ->
                    check_participants(ATag,BTag),
                    send_post_to_owner(ATag,SubElFinal),
                    send_post_to_participants(ATag, BTag, SubElFinal),
                    IQ#iq{type = result,  xmlns =?POSTAUCTIONS,
                          sub_el = [SubElFinal]}
            end;
        get ->
             IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]}
     end.


process_iq_like(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            JTag = xml:get_tag_cdata(xml:get_subtag(SubEl, "user-jid")),
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            case mod_buckets:update_check_bucket(list_to_binary(ATAG),
                                                 list_to_binary("likes"),
                                                 check,
                                                 "",
                                                 some) of
                [] -> 
                    mod_buckets:set_bucket(list_to_binary(ATAG),
                                            list_to_binary("likes"),
                                            list_to_binary(JTag));
                Likes->
                    case  lists:member(list_to_binary(JTag), Likes) of
                        false ->
                            mod_buckets:update_check_bucket(list_to_binary(ATAG),
                                                    list_to_binary("likes"),
                                                    update,
                                                    JTag,
                                                    "");
                        true ->
                            ignore
                    end
            end,
            IQ#iq{type = result, xmlns = ?ALIKE,
                sub_el = [SubEl]};
        get ->
             IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]}
     end.

process_iq_view(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            JTag = xml:get_tag_cdata(xml:get_subtag(SubEl, "user-jid")),
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            case mod_buckets:update_check_bucket(list_to_binary(ATAG),
                                                 list_to_binary("views"),
                                                 check,
                                                 "",
                                                 some) of
                [] -> 
                    mod_buckets:set_bucket(list_to_binary(ATAG),
                                            list_to_binary("views"),
                                            list_to_binary(JTag));
                Likes->
                    case  lists:member(list_to_binary(JTag), Likes) of
                        false ->
                            mod_buckets:update_check_bucket(list_to_binary(ATAG),
                                                    list_to_binary("views"),
                                                    update,
                                                    JTag,
                                                    "");
                        true ->
                            ignore
                    end
            end,
            IQ#iq{type = result, xmlns = ?AVIEW,
                sub_el = [SubEl]};
        get ->
             IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]}
     end.
         
process_iq_bid(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            LTag = xml:get_subtag(SubEl, "lastprice"),
            BTag = xml:get_subtag(SubEl, "bidder"),
            ATag = xml:get_subtag(SubEl, "auction"),
            C =jlib:jid_to_string(_From), 

            {Mega, Secs, _} = now(),
            T = Mega*1000000 + Secs,
            spawn(mod_auction_manager,store_info,[extract_aname(xml:get_tag_cdata(ATag)), 
                                                  xml:get_tag_cdata(BTag), 
                                                  C,
                                                  integer_to_list(T),
                                                  xml:get_tag_cdata(LTag)]),
            Info = [{"lastprice",xml:get_tag_cdata(LTag)},{"bidder",xml:get_tag_cdata(BTag)}],
            mod_auction_manager:store_bidding_history(Info, xml:get_tag_cdata(ATag),integer_to_list(T)),
            mod_muc_room:send_bid(xml:get_tag_cdata(LTag),
                                  xml:get_tag_cdata(ATag),
                                  xml:get_tag_cdata(BTag)),
            IQ#iq{type = result, xmlns = ?ANEW,
                sub_el = [SubEl]};
        get ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]}
    end.


process_iq_check_status(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
            A = xml:get_tag_attr_s("auction", SubEl),
            S = xml:get_tag_attr_s("stream", SubEl),
            R = mod_auction_manager:check_status(S,A),

            IQ#iq{type = result, xmlns = ?CHKAUCTIONS,
                sub_el =[{xmlelement, "query", [{"xmlns", ?CHKAUCTIONS},{"auction", A}], 
                        [{xmlcdata, R }]}]}
    end.

auto_join_auctions_res(User)->
    AuctionNS = [{xmlelement,
            "Bbidder",
             [{"xmlns",
                    "storage:auctionList"}],
            []}],
    Usr = extract_username(User),
    L = mod_private_odbc:get_data(Usr, ?SERVER, AuctionNS),

    [{_,_,_,D}] = L,
    AuctionList = get_list_auctions_loop(D),
    mod_muc_room:tab_bidder(User, AuctionList).

auto_join_auctions(User)->
    AuctionNS = [{xmlelement, 
            "Bbidder",
            [{"xmlns",
                    "storage:auctionList"}],
            []}],
    Usr = extract_username(User),
    L = mod_private_odbc:get_data(Usr, ?SERVER, AuctionNS),
    [{_,_,_,D}] = L,
    AuctionList = get_list_auctions_loop(D),
    lists:foreach(fun(E) ->
                ejabberd_router:route(User,
                    jlib:string_to_jid(E++"/"++Usr), 
                    ({xmlelement,"presence",
                            [{"xml:lang","en"},
                                {"to",E++"/"++Usr}],
                   [{xmlcdata,<<"\n">>},
                    {xmlelement,"priority",[],[{xmlcdata,<<"5">>}]},
                    {xmlcdata,<<"\n">>},
                    {xmlelement,"x",
                        [{"xmlns","http://jabber.org/protocol/muc"}],
                        []},
                    {xmlcdata,<<"\n">>}]}))
    end, AuctionList).

process_iq_search_auctions_all(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
             Stream = xml:get_tag_attr_s("stream_location", SubEl),
                        
            Items = mod_search:get_records_all("stream-auctions-finland", "stream", binary, Stream),            
            IQ#iq{type = result, xmlns = ?SAUCTION ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?SAUCTION},{"stream_location", Stream}],
                        [{xmlelement, "auctions", [],
                                [{xmlcdata,Items }]}
                                
                        ]}]
                        
            }
        end.


increment_views(BucketAuction)->
    Views = mod_auction_manager:get_items(BucketAuction, views),
    [Count] =  Views,
    Current = case binary_to_list(Count) of
        "empty"->"0";
         Other ->
              Other
    end,

    
    case Current of
        "0" ->
            mod_buckets:update_check_bucket(list_to_binary(BucketAuction),
                                                    list_to_binary("views"),
                                                    update_value,
                                                    <<"1">>,
                                                    []),
            "1";
        N ->
            C = list_to_integer(N) + 1,
            NextCount =  integer_to_list(C),
            mod_buckets:update_check_bucket(list_to_binary(BucketAuction),
                                                    list_to_binary("views"),
                                                    update_value,
                                                    list_to_binary(NextCount),
                                                    []),

            NextCount
    end.



get_auction_packet(Auction)->
    Desc = mod_auction_manager:get_item(Auction, "description"),
               Participants = case mod_auction_manager:get_items(Auction, 
                    participants) of 
                        "empty"->
                            "0";
                        [] ->
                            "0";
                        Li ->
                            List = [ I || I <- string:tokens(Li,",")],
                            LikesCount = integer_to_list(length(List)),
                            LikesCount

                    end,

                Views = case mod_auction_manager:get_items(Auction,views) of
                        "empty"->
                                "0";
                        [] ->
                            "0";
                        ViewsCount -> 
                            ViewsCount
                    end,
                    
                Comments = case mod_search:get_comments_all(Auction++"-comment-history", 
                        "auction",binary , Auction) of
                        [] ->
                            "0";
                        C ->
                        integer_to_list(length(C))
                end,
                Title = case mod_auction_manager:get_item(Auction,"title") of
                        "empty"->
                                "";
                        [] ->
                            "";
                        Val -> 
                            Val
                    end,
                Video = case mod_auction_manager:get_item(Auction,"video") of
                    "empty"->
                        "off";
                    [] ->
                        "off";
                    VideoVal->
                        VideoVal
                end,
                CreatedAt = case mod_auction_manager:get_item(Auction,"created_at") of
                        "empty"->
                                "";
                        [] ->
                            "";
                        ValC -> 
                            ValC
                    end,
                Price = mod_auction_manager:get_item(Auction,"asking_price"),
                Type = mod_auction_manager:get_item(Auction,"type"),
                Location = mod_auction_manager:get_item(Auction,"location"),
                AuctionSid = case mod_auction_manager:get_item(Auction,"sid") of
                            [] -> 
                                "";
                            "empty" -> 
                                "";
                            ValSid ->
                                strip_qot(ValSid)
                        end,
                Packet =  [{xmlelement, "auction", 
                            [{"id", Auction},
                             {"created_at",CreatedAt},
                             {"title", Title},
                             {"location", Location},
                             {"description", Desc},
                             {"paricipants", Participants},
                             {"views", Views},
                             {"comments", Comments},
                             {"price", Price},
                             {"video", Video},
                             {"sid", AuctionSid},
                             {"type", Type}], []
                           }],
        Packet.

get_auction_info_list(L)->
    T = lists:foldl(fun(X,A2) ->
                AuctionPacket = get_auction_packet(X), 
                lists:append([A2,AuctionPacket])
        end, [], L),
    T.

get_auction_info(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ)->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
            Auction = xml:get_tag_attr_s("auction", SubEl), 
            PacketAuction = get_auction_packet(Auction),
            Category = mod_auction_manager:get_item(Auction,"category"),
            IQ#iq{type = result, xmlns = ?INFOAUCTION,
                sub_el =[{xmlelement, "query", [{"xmlns", ?INFOAUCTION},{"category", Category}],
                        PacketAuction
                    }]
            }
        end.

process_iq_search_auction_info_list(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
             Criteria = xml:get_tag_attr_s("category", SubEl),
             Number = xml:get_tag_attr_s("range", SubEl),
             Continuation  =  xml:get_tag_attr_s("next_query", SubEl),  
                        
             R  = case Continuation of
                "none" ->
                    mod_search:get_records("category-list", 
                                           "category", 
                                           binary, 
                                           list_to_integer(Number),
                                           Criteria);
                    
                _->
                    mod_search:get_records_next("category-list", "category", binary, 
                                                list_to_integer(Number), 
                                                Continuation, 
                                                Criteria)                    
            end,

            {Items, ExtraInfo} = R,
            ?INFO_MSG("Filter Location, no min, no max~s~n", [R]),
            ?INFO_MSG("Filter Location, no min, no max~s~n", [Criteria]),

            Data =  case length(Items) of
                    0 -> "";
                    _->
                        get_auction_info_list(Items)
                end,
            IQ#iq{type = result, xmlns = ?SDAUCTION ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?SDAUCTION},{"category", Criteria}],
                          [{xmlelement, "auctions", [], Data},
                            {xmlelement, "continuation", [],
                                [{xmlcdata, ExtraInfo }]}
                                
                        ]}]
                        
            }
        end.

process_iq_filter_search(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->

            Max = case xml:get_tag_attr_s("max", SubEl) of
                        [] ->
                            emptyMax;
                        ValMax ->
                            ValMax
            end,
            Min = case xml:get_tag_attr_s("min", SubEl) of
                      [] ->
                            emptyMin;
                      ValMin ->
                            ValMin
            end,
            Location  = case xml:get_tag_attr_s("location", SubEl) of
                     [] -> 
                            emptyLoc;
                     ValLoc -> 
                            ValLoc
            end,

            Criteria = xml:get_tag_attr_s("category", SubEl),
            Number = xml:get_tag_attr_s("range", SubEl),
            Continuation  =  xml:get_tag_attr_s("next_query", SubEl),

            {Result, ExtraInfo} = case Max of
                emptyMax ->
                    case Min of
                        emptyMin ->
                            case Location of
                                emptyLoc -> 
                                    IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
                                _->
                                    ?INFO_MSG("Filter Location, no min, no max~s~n", [Location]),
                                    filter_in_category_loc(Criteria,Location,Number,Continuation)
                            end;
                        _-> 
                            case Location of
                                emptyLoc ->
                                    ?INFO_MSG("Filter Min, no max, no location~s~n", [Min]),
                                    filter_in_category_maxmin_loc(Criteria,Min,[],[],Number,Continuation);
                                _-> %%Filter Min Location, no max
                                    ?INFO_MSG("%%Filter Min Location, no max~s~n", [Min]),
                                    filter_in_category_maxmin_loc(Criteria,Min,[],Location,Number,Continuation)
                            end
                    end;
                _->
                    case Min of
                        emptyMin ->
                            case Location of
                                emptyLoc ->
                                    %%Filter Max, no min, no location
                                    ?INFO_MSG("%%Filter Min Location, no max~s~n", [Min]),
                                    filter_in_category_maxmin(Criteria,[],Max,Number,Continuation);
                                _->
                                    %%Filter  Max Location, no min
                                    ?INFO_MSG("%%Filter Max Location, no min~s~n", [Min]),
                                    filter_in_category_maxmin_loc(Criteria,[],Max, Location,Number,Continuation)
                            end;
                        _->
                            case Location of
                                emptyLoc ->
                                    %%Filter Max Min, no location
                                    ?INFO_MSG("%%FilterMaxMin no Location~s~n", [Max]),
                                    filter_in_category_maxmin(Criteria,Min,Max,Number,Continuation);
                                _->
                                    %%Max,Min,Location 
                                    ?INFO_MSG("Max,Min, No location ~s~n", [Max]),
                                    filter_in_category_maxmin_loc(Criteria,Min,Max,Location,Number,Continuation)
                            end
                    end
            end,

            Data =  get_auction_info_list(Result),
            IQ#iq{type = result, xmlns = ?FILTERS ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?FILTERS},{"category", Criteria}],
                          [{xmlelement, "auctions", [], Data},
                            {xmlelement, "continuation", [],
                                [{xmlcdata, ExtraInfo }]}
                                
                        ]}]
                        
            }
        end.


process_iq_search_auctions(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->
    case Type of
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get ->
             Criteria = xml:get_tag_attr_s("category", SubEl),
             Number = xml:get_tag_attr_s("range", SubEl),
             Continuation  =  xml:get_tag_attr_s("next_query", SubEl),  
                        
             R  = case Continuation of
                "none" ->
                    mod_search:get_records("category-list", 
                                           "category", 
                                           binary, 
                                           list_to_integer(Number),
                                           Criteria);
                    
                _->
                    mod_search:get_records_next("category-list", "category", binary, 
                                                list_to_integer(Number), 
                                                Continuation, 
                                                Criteria)                    
            end,

            {Items, ExtraInfo} = R,
            IQ#iq{type = result, xmlns = ?SAUCTION ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?SAUCTION},{"category", Criteria}],
                        [{xmlelement, "auctions", [],
                                [{xmlcdata,Items }]},
                            {xmlelement, "continuation", [],
                                [{xmlcdata, ExtraInfo }]}
                                
                        ]}]
                        
            }
        end.

get_post_data(L)->
    Data = lists:sort(fun({A,_},{B,_}) -> A =< B end, L),
    [{_,Data1},{_,Data2},{_,Data3}] = Data,
    FinalData = Data1++Data2++Data3,
    FinalData.

get_auction_comment_history(L,Auction)->
    T = lists:foldl(fun(Id,A2) ->
                    %%Get body data
                    BodyData = mod_buckets:update_check_bucket(Auction++"-comment-history", Id, check, [], one),
                    %%Split remaning data
                    [_,TimeData,BidderData] = string:tokens(binary_to_list(Id),"-"),
                    UserData = mod_profile_manager:get_item(BidderData, username),
                    Comments = {xmlelement, "post",
                            [{"type","auction"}],
                            [{xmlelement, "auction", [], [{xmlcdata, Auction}]},
                             {xmlelement, "time", [], [{xmlcdata,TimeData }]},
                             {xmlelement, "bidder", [], [{xmlcdata, BidderData}]},
                             {xmlelement, "username", [], [{xmlcdata, UserData}]},
                             {xmlelement,  "body", [], [{xmlcdata, BodyData}]}
                            ]
                               },
                    lists:append([A2,[Comments]])
            end, [], L),
    T.


retreive_auction_comments(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get->
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            ?INFO_MSG("starting auctions module  v0.0.1~s~n", [ATAG]),
            increment_views(ATAG),
            ListKeys = mod_search:get_comments_records(ATAG, [],[],[]),
            Data =  get_auction_comment_history(ListKeys, ATAG),
         IQ#iq{type = result, xmlns = ?SAUCTION ,
               sub_el =[{xmlelement, "query", [{"xmlns", ?ACOMMENTHISTORY }],Data}]
              }
    end.


filter_in_category_maxmin_loc(Category, 
                              _Range1, 
                              _Range2, 
                              Location, 
                              Limit, 
                              _Continuation) ->
    R  = case _Continuation of
                "none" ->
                    mod_search:get_records("category-list", 
                                           "category", 
                                           binary, 
                                           list_to_integer(Limit),
                                           Category);
                    
                _->
                    mod_search:get_records_next("category-list", "category", binary, 
                                                list_to_integer(Limit), 
                                                _Continuation,
                                                Category)                 
            end,

    {Items, ExtraInfo} = R,
    CheckMin = case _Range1 of
        [] ->
            false;
        _-> true
    end,
                
    CheckMax  = case _Range2 of
        [] ->
            false;
        _-> true
    end,
    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [CheckMin]),
    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [CheckMax]),
    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [Items]),
    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [ExtraInfo]),
    List = lists:foldl(fun(X,AList) ->
                    CheckPrice = mod_auction_manager:get_item(X, "asking_price"),
                    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [X]),
                    ?INFO_MSG("starting auctions module  v0.0.1~s~n", [CheckPrice]),
                    case CheckMin of
                                true ->
                                    ?INFO_MSG("starting auctions module CASE Min exists v0.0.1~s~n", [CheckMin]),
                                    case (list_to_integer(CheckPrice) >= list_to_integer(_Range1))of
                                        true ->
                                        ?INFO_MSG("starting auctions module CASE min grater v0.0.1~s~n", [true]),
                                            case CheckMax of
                                                true->
                                                ?INFO_MSG("starting auctions module CASE Max exists v0.0.1~s~n", [CheckMax]),
                                                     case (list_to_integer(CheckPrice) =< list_to_integer(_Range2)) of
                                                        true ->
                                                    ?INFO_MSG("starting auctions module CASE max minor v0.0.1~s~n", [true]),
                                                              CheckLocation = mod_auction_manager:get_item(X, "location"),
                                                                ?INFO_MSG("starting auctions module  v0.0.1~p~n", [CheckLocation]),
                                                              case CheckLocation of
                                                                    Location ->
                                                                        ?INFO_MSG("starting auctions module LOCATION  v0.0.1~p~n", [true]),
                                                                             lists:append([AList,[X]]);
                                                                   _->
                                                                      AList
                                                              end;
                                                         _-> 
                                                            AList
                                                     end;
                                                _->
                                                     CheckLocation = mod_auction_manager:get_item(X, "location"),
                                                              case CheckLocation of
                                                                    Location ->
                                                                             lists:append([AList,[X]]);
                                                                   _->
                                                                      AList
                                                                end
                                            end;
                                        _-> 
                                            AList
                                    end;
                                _->
                                     case (list_to_integer(CheckPrice) =< list_to_integer(_Range2)) of
                                                    true ->
                                                        CheckLocation = mod_auction_manager:get_item(X, "location"),
                                                            case CheckLocation of
                                                                Location ->
                                                                    lists:append([AList,[X]]);
                                                                   _->
                                                                    AList
                                                            end;
                                                         _-> 
                                                            AList
                                     end
                                                     
                    end
            end, [], Items),
    ?INFO_MSG("starting auctions module  v0.0.1~p~n", [List]),
    ?INFO_MSG("starting auctions module  v0.0.1~p~n", [ExtraInfo]),
    {List, ExtraInfo}.

filter_in_category_maxmin(Category, 
                              _Range1, 
                              _Range2, 
                              Limit, 
                              _Continuation) ->
    R  = case _Continuation of
                "none" ->
                    mod_search:get_records("category-list", 
                                           "category", 
                                           binary, 
                                           list_to_integer(Limit),
                                           Category);
                    
                _->
                    mod_search:get_records_next("category-list", "category", binary, 
                                                list_to_integer(Limit), 
                                                _Continuation,
                                                Category)                 
            end,

    {Items, ExtraInfo} = R,
    CheckMin = case _Range1 of
        [] ->
            false;
        _-> true
    end,
                
    CheckMax  = case _Range2 of
        [] ->
            false;
        _-> true
    end,
    
    List = lists:foldl(fun(X,AList) ->
                    CheckPrice = mod_auction_manager:get_item(X, "asking_price"),
                    case CheckMin of
                                true ->
                                    case (list_to_integer(CheckPrice) >= list_to_integer(_Range1))of
                                        true ->
                                            case CheckMax of
                                                true->
                                                     case (list_to_integer(CheckPrice) =< list_to_integer(_Range2)) of
                                                        true ->
                                                                lists:append([AList,[X]]);
                                                         _-> 
                                                            AList
                                                     end;
                                                _->
                                                    lists:append([AList,[X]])
                                             end;
                                        _-> 
                                            AList
                                    end;
                                _->
                                    case CheckMax of
                                                true->
                                                     case (list_to_integer(CheckPrice) =< list_to_integer(_Range2)) of
                                                        true ->
                                                                lists:append([AList,[X]]);
                                                         _-> 
                                                            AList
                                                     end;
                                                _->
                                                    AList
                                    end
                    end
            end, [], Items),
    {List, ExtraInfo}.

filter_in_category_loc(Category, 
                       Location, 
                       Limit, 
                       _Continuation) ->
    R  = case _Continuation of
                "none" ->
                    mod_search:get_records("category-list", 
                                           "category", 
                                           binary, 
                                           list_to_integer(Limit),
                                           Category);
                    
                _->
                    mod_search:get_records_next("category-list", "category", binary, 
                                                list_to_integer(Limit), 
                                                _Continuation,
                                                Category)                 
            end,
    {Items, ExtraInfo} = R,
    List = lists:foldl(fun(X,AList) ->
                    CheckLocation = mod_auction_manager:get_item(X, "location"),
                    case CheckLocation of
                        Location ->
                            lists:append([AList,[X]]);
                        _->
                            AList
                    end
            end, [], Items),
    {List, ExtraInfo}.

