-module(mod_profile_manager).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").

-define(B1, "username").
-define(B2, "lastseen").
-define(B3,  "auctionsown").
-define(B4,  "acutionsfollowing").

-export([start/2, stop/1]).
-export([init_items/1,
        store_profile_data/5,
        update_profile_item_data/3,
        get_item/2,
        get_items/2,
        delete_items/1]).

start(_Host, _Opts) ->
     ?INFO_MSG("starting mod_profile_manager v1", []),
     ?INFO_MSG("starting mod_profile_manager v1", []).
stop(_Host) ->
    ?INFO_MSG("stopping mod_profile_manager v1 ", []).


init_items(Name) ->
    Buckets =  [?B1,?B2,?B3,?B4],
    lists:foreach(fun(A) ->
            mod_buckets:set_bucket(list_to_binary(Name),
                list_to_binary(A),
                <<"empty">>)
        end, Buckets).

delete_items(Name) ->
    Buckets =  [?B1,?B2,?B3,?B4],
    lists:foreach(fun(A) ->
            mod_buckets:delete_bucket(list_to_binary(Name),list_to_binary(A))
    end, Buckets).

get_item(BName, KName) ->
    case KName of
            username -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"username">>,
                                                    check,
                                                    "",
                                                    one);
             lastseen -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                        <<"lastseen">>,
                                                          check,
                                                          "",
                                                          one)
    end.


get_items(BName, KName) ->
    case KName of
       auctionsown -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                   <<"auctionsown">>,
                                                    check,
                                                    "",
                                                    one);
        auctionsfollowing ->  mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"auctionsfollwing">>,
                                                    check,
                                                    "",
                                                    one)
    end.
item(username, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name), 
        list_to_binary(atom_to_list(username)), 
        update_value, 
        list_to_binary(Item), 
        []);
item(lastseen, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name), 
        list_to_binary(atom_to_list(lastseen)), 
        update_value,
        list_to_binary(Item), 
        []);
item(auctionsown, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(auctionsown)),
        update_value,
        list_to_binary(Item),
        []);
item(auctionsfollowing, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(auctionsfollowing)),
        update_value,
        list_to_binary(Item),
        []).

update_profile_item_data(AName, Item, Value) ->
    case Item of
        username ->
            item(username, Value, AName);
        lastseen ->
            item(lastseen, Value, AName);
        auctionsown ->
            item(auctionsown, Value, AName);
        auctionsfollowing->
            item(auctionsfollowing, Value, AName)
    end.

store_profile_data(Name, _V1, _V2, _V3, _V4) ->
    case _V1 of
        [] -> ignore;
        _->
            item(username, _V1, Name)
    end,
    case _V2 of
        [] -> ignore;
        _->
            item(lastseen, _V2, Name)
    end,
    case _V3 of
        [] -> ignore;
        _->
            item(auctionsown, _V3, Name)
    end,
    case _V4 of
        [] -> ignore;
        _->
            item(auctionsfollowing, _V4, Name)
    end.        
