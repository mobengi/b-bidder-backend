-module(mod_lastseen).
-behaviour(gen_mod).
-export([start/2,
	 stop/1,
	 on_presence_update/4,
	 store_last_info/4,
	 remove_user/2]).

-include("ejabberd.hrl").
-include("jlib.hrl").
start(Host, Opts) ->
    IQDisc = gen_mod:get_opt(iqdisc, Opts, one_queue),
    ejabberd_hooks:add(remove_user, Host,
		       ?MODULE, remove_user, 50),
    ejabberd_hooks:add(unset_presence_hook, Host,
		       ?MODULE, on_presence_update, 50).

stop(Host) ->
    ejabberd_hooks:delete(remove_user, Host,
			  ?MODULE, remove_user, 50),
    ejabberd_hooks:delete(unset_presence_hook, Host,
			  ?MODULE, on_presence_update, 50).

now_to_seconds({MegaSecs, Secs, _MicroSecs}) ->
    MegaSecs * 1000000 + Secs.


on_presence_update(User, Server, _Resource, Status) ->
    TimeStamp = now_to_seconds(now()),
    ?INFO_MSG("ONPRECENCE UPDATEEE ~p~n",[TimeStamp]),
    store_last_info(User, Server, TimeStamp, Status).

store_last_info(User, Server, TimeStamp, Status) ->
    LUser = jlib:nodeprep(User),
    Username = ejabberd_odbc:escape(LUser),
    Seconds = ejabberd_odbc:escape(integer_to_list(TimeStamp)),
    mod_profile_manager:update_profile_item_data(Username,lastseen, Seconds).

remove_user(User, Server) ->
    LUser = jlib:nodeprep(User),
    LServer = jlib:nameprep(Server),
    Username = ejabberd_odbc:escape(LUser),
    ok.
