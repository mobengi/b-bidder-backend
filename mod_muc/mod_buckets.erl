-module(mod_buckets).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").

-export([start/2, stop/1]).
-export([set_bucket/3,
        set_indexed_bucket/5,
        connect/0,
        disconnect/1,
        get_number/1,
        delete_bucket/2,
        update_check_bucket/5, 
        get_keys/1]).

-define(DOMAIN, "gimmick.fi").
-define(NODE_IP, "146.185.156.173").
-define(NODE_PORT, 8087).


start(_Host, _Opts) ->
    ?INFO_MSG("starting mod_buckets v0.0.1", []),
    ok.

stop(_Host) ->
    ?INFO_MSG("stopping mod_buckets v0.0.1 ", []),
    ok.

connect()->
    riakc_pb_socket:start_link(?NODE_IP, ?NODE_PORT).

disconnect(Pid)->
    riakc_pb_socket:stop(Pid).

get_keys(Name)->
    {ok, Pid} = connect(),
    NameBinary = list_to_binary(Name),
    {ok, Keys} = riakc_pb_socket:list_keys(Pid, NameBinary),
    disconnect(Pid),
    Keys.

get_number(Name) ->
    {ok, Pid} = connect(),
    NameBinary =  list_to_binary(Name),
    {ok, Keys} = riakc_pb_socket:list_keys(Pid, NameBinary),
    disconnect(Pid),
    Number = erlang:size(list_to_tuple(Keys)),
    Number.

delete_bucket(Name, Key) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    riakc_pb_socket:delete(Pid, N, K),
    disconnect(Pid).

set_bucket(Name, undefined , Data)->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    Obj = riakc_obj:new(N, undefined, Data),
    riakc_pb_socket:put(Pid, Obj),
    disconnect(Pid);

set_bucket(Name, Key, Data) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    Obj = riakc_obj:new(N, K, Data),
    riakc_pb_socket:put(Pid, Obj),
    disconnect(Pid).

update_check_bucket(Name, Key, Command, _Value, _Number) ->
    {ok, Pid} = connect(),
    R = case Command of
        check ->
            Result  = riakc_pb_socket:get(Pid, Name, Key),
            case Result of
                {ok, O} ->
                    V =  case _Number of 
                    some ->riakc_obj:get_values(O);
                    one ->binary_to_list(riakc_obj:get_value(O))
                    end,
                    V;
                {error, notfound} ->
                    ""
            end;
        update ->
            set_bucket(Name , Key, list_to_binary(_Value));
        update_value ->
            {ok, O} = riakc_pb_socket:get(Pid, Name, Key),
            ONew = riakc_obj:update_value(O, _Value),
            {ok, OFinal} = riakc_pb_socket:put(Pid, ONew, [return_body]),
            OFinal
    end,
    disconnect(Pid),
    R.

set_indexes(Bucket, Type, _N1, _N2, _N3, _V1, _V2 ,_V3)->
    O1 = riakc_obj:get_update_metadata(Bucket),
    
    O2 = case Type of
        integer ->
            riakc_obj:set_secondary_index(O1,
                [{{integer_index,_N1 }, [_V1]}]);
        binary ->
            riakc_obj:set_secondary_index(O1,
                [{{binary_index, _N1}, [_V1]},
                 {{binary_index, _N2}, [_V2]}]);
        multi->
            riakc_obj:set_secondary_index(O1,
                [{{binary_index, _N1}, [_V1]},
                 {{binary_index, _N2}, [_V2]},
                 {{integer_index, _N3}, [_V3]}])

    end,
    riakc_obj:update_metadata(Bucket,O2).

set_indexed_bucket(Name, Key, Data, 
        Type, Indexes) ->
    {ok, Pid} = connect(),
    ?INFO_MSG("PID CONNECT ~p~n",[Pid]),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    [_N1, _N2, _N3, _V1, _V2, _V3] = Indexes,
    Obj = riakc_obj:new(N, K, Data),
    Obj2 = set_indexes(Obj, Type, _N1, _N2, _N3, _V1, _V2, _V3),
    riakc_pb_socket:put(Pid, Obj2),
    disconnect(Pid).
