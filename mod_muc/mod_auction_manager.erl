-module(mod_auction_manager).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").

-define(B1, "bidder").
-define(B2, "time").
-define(B3, "video").
-define(B4, "last_bid").
-define(B5, "owner_id").
-define(B6, "created_at").
-define(B7, "-bidding-history").
-define(B8, "-comment-history").
-define(B9, "description").
-define(B10, "title").
-define(B11, "type").
-define(B12, "location").
-define(B13, "asking_price").
-define(B14,  "participants").
-define(B15,  "likes").
-define(B16,  "views").
-define(B17,  "category").
-define(B20, "-offers").
-define(B18, "opentokSessionId").
-define(B19, "opentokValidToken").

-export([start/2, stop/1]).
-export([init_items/1,
        store_auction_data/8,
        update_auction_data/3,
        get_item/2,
        delete_auction_data/1,
        get_items/2,
        store_comment/4,
        check_status/2,
        get_offer_items/1,
        store_offer/5]).

start(_Host, _Opts) ->
     ?INFO_MSG("starting mod_auction_manager v1", []),
     ?INFO_MSG("starting mod_auction_manager v1", []).
stop(_Host) ->
    ?INFO_MSG("stopping mod_auction_manager v1 ", []).


init_items(Name) ->
    Buckets =  [?B1,?B2,?B3,?B4,?B5,?B6,?B7,?B8,?B9,
        ?B10,?B11,?B12,?B13,?B15,?B16,?B17],
    lists:foreach(fun(A) ->
            mod_buckets:set_bucket(list_to_binary(Name),
                list_to_binary(A),
                <<"empty">>)
        end, Buckets).

delete_auction_data(Auction) ->
    AuctionKeys =  [?B1,?B2,?B3,?B4,?B5,?B6,?B7,?B8,?B9,?B10,
                ?B11,?B12,?B13,?B14,?B15,?B16,?B17,?B18,?B19],
    lists:foreach(fun(A) ->
            mod_buckets:delete_bucket(list_to_binary(Auction),list_to_binary(A))
    end, AuctionKeys),
    CommentHistoryKeys = mod_search:get_comments_records(Auction, [],[],[]),
    lists:foreach(fun(B) ->
            mod_buckets:delete_bucket(list_to_binary(Auction++?B8),B)
    end, CommentHistoryKeys),
    OfferHistoryKeys = mod_search:get_offers_all(Auction++?B20,binary, Auction),
    lists:foreach(fun(C) ->
            mod_buckets:delete_bucket(list_to_binary(Auction++?B20),C)
    end, OfferHistoryKeys).
    
get_item(BName, KName) ->
    case KName of
            "category" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"category">>,
                                                    check,
                                                    "",
                                                    one);
            "owner_id" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"owner_id">>,
                                                    check,
                                                    "",
                                                    one);
            "time" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                  <<"time">>,
                                                  check,
                                                  "",
                                                  one);
            "video" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                <<"video">>,
                                                check,
                                                "",
                                                one);
            "created_at" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                <<"created_at">>,
                                                check,
                                                "",
                                                one);
            "last_bid" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                <<"last_bid">>,
                                                check,    
                                                "",
                                                one);
            "title" ->  mod_buckets:update_check_bucket(list_to_binary(BName),
                                                           <<"title">>,
                                                          check,
                                                          "",
                                                          one);
            "description" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                           <<"description">>,
                                                          check,
                                                          "",
                                                          one);
            "type" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                           <<"type">>,
                                                          check,
                                                          "",
                                                          one);
             "location" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                        <<"location">>,
                                                          check,
                                                          "",
                                                          one);
             "asking_price" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                        <<"asking_price">>,
                                                          check,
                                                          "",
                                                          one);
             "sid" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                        <<"opentokSessionId">>,
                                                          check,
                                                          "",
                                                          one);
             "tok" -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                        <<"opentokValidToken">>,
                                                          check,
                                                          "",
                                                          one)

    end.


get_offer_items(Auction) ->
    Bucket = Auction ++ "-offers",
    Res = mod_search:get_offers_all(Bucket, "offer-history", binary, Auction),
    Res.

get_items(BName, KName) ->
    case KName of
       likes -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                   <<"likes">>,
                                                    check,
                                                    "",
                                                    one);
        views -> mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"views">>,
                                                    check,
                                                    "",
                                                    some);
        comments ->  mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"-comment-history">>,
                                                    check,
                                                    "",
                                                    some);
        participants ->  mod_buckets:update_check_bucket(list_to_binary(BName),
                                                    <<"participants">>,
                                                    check,
                                                    "",
                                                    one)
    end.

item(video, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name), 
        list_to_binary(atom_to_list(video)), 
        update_value, 
        list_to_binary(Item), 
        "");
item(category, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name), 
        list_to_binary(atom_to_list(category)), 
        update_value, 
        list_to_binary(Item), 
        "");
item(owner_id, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name), 
        list_to_binary(atom_to_list(owner_id)), 
        update_value,
        list_to_binary(Item), 
        "");

item(time, Item, Name) ->
    mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(time)),
        update_value,
        list_to_binary(Item),
        "");
item(last_bid, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(last_bid)),
        update_value,
        list_to_binary(Item),
        "");
item(type, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(type)),
        update_value,
        list_to_binary(Item),
        "");
item(description, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(description)),
        update_value,
        list_to_binary(Item),
        "");
item(title, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(title)),
        update_value,
        list_to_binary(Item),
        "");
item(location, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(location)),
        update_value,
        list_to_binary(Item),
        "");
item(asking_price, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(asking_price)),
        update_value,
        list_to_binary(Item),
        "");
item(likes, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(likes)),
        update_value,
        list_to_binary(Item),
        "");
item(views, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(views)),
        update_value,
        list_to_binary(Item),
        "");
item(created_at, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(created_at)),
        update_value,
        list_to_binary(integer_to_list(Item)),
        "");
item(participants, Item, Name) ->
     mod_buckets:update_check_bucket(
        list_to_binary(Name),
        list_to_binary(atom_to_list(participants)),
        update_value,
        list_to_binary(Item),
        "").

update_auction_data(AName, Item, Value) ->
    case Item of
        video ->
            item(video, Value, AName);
        title ->
            item(title, Value, AName);
        description ->
            item(description, Value, AName);
        type->
            item(type, Value, AName);
        asking_price->
            item(asking_price, Value, AName);
        likes ->
            item(likes, Value, AName);
        views ->
            item(views, Value, AName);
        created_at ->
            item(created_at, Value, AName);
        participants->
            item(participants, Value, AName)
    end.

store_auction_data(Name, 
        _V1, 
        _V2, 
        _V3, 
        _V4, 
        _V5, 
        _V6, 
        _V7) ->
    case _V1 of
        [] -> ignore;
        _->
            item(title, _V1, Name)
    end,
    case _V2 of
        [] -> ignore;
        _->
            item(description, _V2, Name)
    end,
    case _V3 of
        [] -> ignore;
        _->
            item(location, _V3, Name)
    end,        
    case _V4 of
        [] -> ignore;
        _->
            item(owner_id, _V4, Name)
    end,
    case _V5 of
        [] -> ignore;
        _->
            item(asking_price, _V5, Name)
    end,
    case _V6 of
        [] ->
            ignore;
        _->
            item(created_at, _V6, Name)
    end,
    case _V7 of
        [] ->
            ignore;
        _->
            item(category, _V7, Name)
    end.


check_status(SName, AName) ->
    R = mod_buckets:update_check_bucket(list_to_binary(SName), 
                                    list_to_binary(AName), 
                                    check,"", one),
    case R of 
        [] ->
            "does-not-exist";
        R ->
            "active"
    end.

store_offer(Auction, Bidder, Type, Price, TimeList) ->
    TimeInt = list_to_integer(TimeList),
    mod_buckets:set_indexed_bucket(
        Auction++?B20, 
        Auction++"-"++TimeList++"-"++Bidder++"-"++Type,
        list_to_binary(Price),
        multi,
        ["auction","bidder", "time",
            list_to_binary(Auction),
            list_to_binary(Bidder),
            TimeInt]).

store_comment(Auction, Body, Bidder, TimeList)->
    TimeInt = list_to_integer(TimeList),
    mod_buckets:set_indexed_bucket(Auction++?B8, 
        Auction++"-"++TimeList++"-"++Bidder,
        list_to_binary(Body),
        multi,
        ["auction","bidder", "time",
            list_to_binary(Auction),
            list_to_binary(Bidder),
            TimeInt]).
