-module(mod_offers).
-behaviour(gen_mod).
-include("jlib.hrl").
-include("ejabberd.hrl").
-export([start/2, stop/1]).
-export([set_offer/3, get_offers/3, get_offer_history/2]).
%%IQ names-spaces
-define(GET_OFFER,"get-offer").
-define(SET_OFFER,"set-offer").
-define(AOFFERSHISTORY,"offers-history").
-define(SERVER,"gimmick.fi").
-define(OFFERBUCKET, "-offers").

start(_Host, _Opts) ->
    IQDisc = gen_mod:get_opt(iqdisc, _Opts, one_queue),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?SET_OFFER, ?MODULE, set_offer, IQDisc),
    gen_iq_handler:add_iq_handler(ejabberd_local, _Host, ?GET_OFFER, ?MODULE, get_offers, IQDisc),
    ok.

stop(_Host) ->
    ok.
set_offer(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            
            TTAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "type")),
            PTAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "price")),
            %CTAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "comment")),
            ATAG = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            Time = integer_to_list(mod_auction_triggers:get_timestamp()),

            %%TODO enable or disable comment later.
            Bi = mod_auctions:extract_username(_From), 
            mod_auction_manager:store_offer(ATAG, Bi, TTAG, PTAG, Time),
            Usr = mod_profile_manager:get_item(Bi, username),
            send_offer_participants(ATAG, TTAG, PTAG, Time, Bi, Usr),
            IQ#iq{type = result, xmlns = ?SET_OFFER ,
                sub_el =[{xmlelement, "query", [{"xmlns", ?SET_OFFER}],
                        [{xmlelement, "type", [],[{xmlcdata, TTAG }]},
                         {xmlelement, "price", [],[{xmlcdata, PTAG }]},
                         %%{xmlelement, "comment", [],[{xmlcdata, CTAG }]},
                         {xmlelement, "auction", [],[{xmlcdata, ATAG }]},
                         {xmlelement, "time", [],[{xmlcdata, Time }]},
                         {xmlelement, "bidder", [],[{xmlcdata, Bi }]},
                          {xmlelement, "username", [],[{xmlcdata, Usr }]}
                        ]}]
                        
            };
        get->
            ignore
    end.

send_offer_participants(Auction, 
                        Type, 
                        Price,
                        Time,
                        Bidder,
                        Usr) ->
	Packet = {xmlelement, "iq",[{"type", "result"}],[{xmlelement, "query", [{"xmlns", ?SET_OFFER}],
                        [{xmlelement, "type", [],[{xmlcdata, Type }]},
                         {xmlelement, "price", [],[{xmlcdata, Price }]},
                         {xmlelement, "auction", [],[{xmlcdata, Auction }]},
                         {xmlelement, "time", [],[{xmlcdata, Time }]},
                         {xmlelement, "bidder", [],[{xmlcdata, Bidder }]},
                          {xmlelement, "username", [],[{xmlcdata, Usr }]}
                        ]}]},

            %%%GET PARTICIPANTS
            PData=mod_auction_manager:get_items(Auction, participants),
            List = [ I || I <- string:tokens(PData,",")],
            lists:foreach(fun(P) ->
                    case P of
                        Bidder ->
                            ignore;
                        _->
                            User = P++"@"++?SERVER++"/"++P,
                            mod_notifier:notify_offer(P,xml:element_to_string(Packet)),
                            ejabberd_router:route(jlib:string_to_jid(?SERVER),
                                              jlib:string_to_jid(User),
                                              Packet)
                    end
                end, List).

get_offer_history(L,Auction)->
    T = lists:foldl(fun(Id,A2) ->
                    %%Get body data
                    PriceData = mod_buckets:update_check_bucket(Auction++?OFFERBUCKET, Id, check, [], one),
                    %%Split remaning data
                    [_,TimeData,BidderData,TypeData] = string:tokens(binary_to_list(Id),"-"),
                    UserData = mod_profile_manager:get_item(BidderData, username),
                    Offers = {xmlelement, "offer",
                            [],
                            [{xmlelement, "auction", [], [{xmlcdata, Auction}]},
                             {xmlelement, "time", [], [{xmlcdata,TimeData }]},
                             {xmlelement, "type", [], [{xmlcdata,TypeData }]},
                             {xmlelement, "bidder", [], [{xmlcdata, BidderData}]},
                             {xmlelement, "username", [], [{xmlcdata, UserData}]},
                             {xmlelement,  "price", [], [{xmlcdata, PriceData}]}
                            ]
                               },
                    lists:append([A2,[Offers]])
            end, [], L),
    T.

get_offers(_From, _To, #iq{type = Type, sub_el = SubEl} = IQ) ->

    case Type of 
        set ->
            IQ#iq{type = error, sub_el = [SubEl, ?ERR_NOT_ALLOWED]};
        get->
            AuctionTag = xml:get_tag_cdata(xml:get_subtag(SubEl, "auction")),
            OffersKeys = mod_search:get_offers_all(AuctionTag++?OFFERBUCKET,binary,AuctionTag),
            Offers =  get_offer_history(OffersKeys,AuctionTag),
            IQ#iq{type = result, xmlns = ?GET_OFFER,
                sub_el =[{xmlelement, "query", [{"xmlns", ?AOFFERSHISTORY }],
                        Offers}]
              }
    end.

